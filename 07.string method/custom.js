var fruit = "আমাদের জাতীয় ফল কাঠাল";
document.write(fruit);
document.write("<br>");


// string.length
document.write(fruit.length);
document.write("<br>");

// string.indexOf()
var indexno = fruit.indexOf("কাঠাল");
document.write(indexno,' থেকে কাঠাল লেখাটি শু্রু হয়েছে এখানে ',indexno,' একটি ইনডেক্স নাম্বার যা 0 থেকে শুরু হয় ।');
document.write("<br>");

// slice(param1, param2);
document.write(fruit.slice(16,21));
document.write("<br>");

// replace(param1, param2);
document.write(fruit.replace("আমাদের জাতীয়","আমার প্রিয়"));
document.write("<br>");

var myName = "Akter Jahan";
//toUpperCase();
document.write(myName.toUpperCase());
document.write("<br>");

//toLowerCase()
document.write("ABU BASED".toLowerCase());
document.write("<br>");

//charAt()
document.write(fruit.charAt(16));
